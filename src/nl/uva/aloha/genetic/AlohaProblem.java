package nl.uva.aloha.genetic;

import java.util.function.Function;

import espam.datamodel.graph.cnn.Network;
import espam.datamodel.graph.csdf.datasctructures.CSDFEvalResult;
import espam.interfaces.python.Espam2DARTS;
import io.jenetics.engine.Codec;
import io.jenetics.engine.Problem;
import io.jenetics.ext.moea.Vec;
import nl.uva.aloha.Configs;
import nl.uva.aloha.helpers.OnnxHelper;
import nl.uva.aloha.helpers.SatelliteEvaluator;

import org.json.simple.JSONObject;

import nl.uva.aloha.helpers.RedisDB;

import nl.uva.aloha.helpers.SearchResultCollector;
/**
 * 
 * @author Dolly Sapra
 *
 * This class encapsulates the genotype and where to look for (multi-objective) evaluations.
 * All evaluations results for each genotype are stored in a vector (of type double) 
 */
public class AlohaProblem implements Problem<GenotypeTranslation, LayerGene, Vec<double[]>> {

	private static final int NUM_OBJECTIVES = 3; //1-Accuracy //2-Memory //3-Energy
	
	@Override
	public Codec<GenotypeTranslation, LayerGene> codec() 
	{
		return new DNNCodec();
	}

	@Override
	public Function<GenotypeTranslation, Vec<double[]>> fitness()
	{
		return this::fitnessEvaluations;
	}

	private Vec<double[]> fitnessEvaluations(GenotypeTranslation gtTrans)
	{
		final double[] fitnessValues = new double[NUM_OBJECTIVES];
		
		SearchResultCollector srCol = gtTrans.getSearchResultCollector();
		System.out.println("srCol");
		System.out.println(srCol);
		System.out.println(srCol.id);
		String label = Configs.projectId + ":GAONNX:path:"+srCol.id;
		
			try
			{
		RedisDB.set(label,srCol.relativePath);
		}
		catch(Exception e)
			{
				System.out.println("No connection to RedisDB available.");
				}
//	  JSONObject resp=null;
//	  
//		resp = SatelliteEvaluator.evaluateAccuracy(gtTrans.getSearchResultCollector());
//		String acc_jobid =  (String)resp.get("job_id");
//		String acc_cbid  =  (String)resp.get("callback_id");
//		
//		
//		resp = SatelliteEvaluator.evaluateHardware(gtTrans.getSearchResultCollector());
//		String perf_jobid =  (String)resp.get("job_id");
//		String perf_cbid  =  (String)resp.get("callback_id");
		double acc=0.0;
		double mem=0.0;
		double ene=0.0;
		
		String res_key = label.replace ("path","res");
		try{
			while (! RedisDB.exists(res_key+":acc")){
			Thread.sleep(5000);
			}
			acc = Double.parseDouble(RedisDB.get(res_key+":acc"));
			RedisDB.delete(res_key+":acc");
		
			while (! RedisDB.exists(res_key+":mem")){
				Thread.sleep(5000);
			}
			mem = Double.parseDouble(RedisDB.get(res_key+":mem"));
			RedisDB.delete(res_key+":mem");
		
			while (! RedisDB.exists(res_key+":ene")){
				Thread.sleep(5000);
			}
			ene = Double.parseDouble(RedisDB.get(res_key+":ene"));
			RedisDB.delete(res_key+":ene");
		}
		catch(Exception e)
		{
				System.out.println("No connection to RedisDB available. Using fake results!");
				acc = 56.0;
				mem= 424234;
				ene = 234;
				try {
				Thread.sleep(5000);
				}
				catch(Exception ex){
				System.err.print(ex.getStackTrace());
				}
		}
		
    System.out.println("ooooooooooooooo4");
		fitnessValues[0] =  (1.0 -  acc);
		fitnessValues[1] =          mem; 
		fitnessValues[2] =          ene;
		
		return Vec.of(fitnessValues);
	}
	
		
		
		
		
		
		
		
		
		
		
		
		
	
	
	@SuppressWarnings("unused")
	private Vec<double[]> localFitnessEvaluations(GenotypeTranslation gtTrans)
	{
		final double[] fitnessValues = new double[NUM_OBJECTIVES];
		
		double accuracy = localAccuracyFitness(gtTrans.getOnnxName());
		gtTrans.setAccuracy(accuracy);
		fitnessValues[0] = (1.0 - accuracy ); //GA engine is set to minimizing - So minimize the error. 
		
		CSDFEvalResult evalRes = localHardwarefitness(gtTrans.getNetwork());
		gtTrans.setHardwareEval(evalRes);
		if(evalRes==null)
		{
			fitnessValues[1] = Double.MAX_VALUE;
			fitnessValues[2] = Double.MAX_VALUE;
		}
		else
		{
			fitnessValues[1] = evalRes.getMemory();
			fitnessValues[2] = evalRes.getEnergy();
		}
		
		System.out.println(gtTrans.toEvaluatedString());
		
		return Vec.of(fitnessValues);
	}
	
	
	private static double localAccuracyFitness(String onnxName) 
	{
		try
		{
		 Double accuracyResult = OnnxHelper.evaluateOnnx(onnxName);
		 if(accuracyResult!=null)
			{
				 return accuracyResult;
			}
			else 
			{
				//System.err.println("Accuracy Result is null");
				return 0.0;
			}
		}
		catch(Exception e)
		{
			//System.err.print(e.getStackTrace());
			return 0.0;
		}
			
	
	}
	
	
	private CSDFEvalResult localHardwarefitness(Network network) 
	{
		if(network.checkConsistency())
		{
			try
			{
				CSDFEvalResult evalResult =  new Espam2DARTS().evaluateCNN(network);
				return evalResult;  //evalResult.getPerformance(); //Double.MAX_VALUE;
				
			}
			catch(Exception e)
			{
				System.err.print(e.getStackTrace());
				return null;
			}
		}
		else
		{
			System.err.print("Error: This should never be printed ideally");
			return null;
		}
	}
	
	
}
