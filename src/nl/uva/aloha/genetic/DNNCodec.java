package nl.uva.aloha.genetic;

import java.util.function.Function;

import io.jenetics.Genotype;
import io.jenetics.engine.Codec;
import io.jenetics.util.Factory;
import io.jenetics.util.IntRange;
import nl.uva.aloha.Configs;

public class DNNCodec implements Codec<GenotypeTranslation, LayerGene>
{

	public static final Genotype<LayerGene> ENCODING = Genotype.of(
			SimpleLayerChromosome.of("dataI",1,2),
			DualLayerChromosome.of("Convolution:ReLu", 32, 64,IntRange.of(4,7)), // Min is inclusive, Max is exclusive for range
			SimpleLayerChromosome.of("Pooling",5,50),
			DualLayerChromosome.of("Convolution:ReLu", 32, 128,IntRange.of(6,9)),
			SimpleLayerChromosome.of("Pooling",5,50),
			DualLayerChromosome.of("Convolution:ReLu", 64, 320,IntRange.of(6,9)),
			SimpleLayerChromosome.of("GlobalAveragePool",5,50),
			SimpleLayerChromosome.of("DenseBlock",100,900,IntRange.of(1, 2)),
			SimpleLayerChromosome.of("DenseBlock",10,10), //LAST layer is fully connected layer, num of neurons = number of output classes
			SimpleLayerChromosome.of("Softmax",10,10),
			SimpleLayerChromosome.of("dataO",1,2)
		);
	
	public static final Genotype<LayerGene> PAMAP2ENCODING = Genotype.of(
			SimpleLayerChromosome.of("dataI",1,2),
			DualLayerChromosome.of("Convolution:ReLu", 32, 96,IntRange.of(4,7)), // 2-4 //Min is inclusive, Max is exclusive for range
			SimpleLayerChromosome.of("Pooling",5,50),
			DualLayerChromosome.of("Convolution:ReLu", 128, 192,IntRange.of(4,9)),//2-5
			SimpleLayerChromosome.of("GlobalMaxPool",5,50),
			DualLayerChromosome.of("DenseBlock:ReLu",200,600,IntRange.of(2, 5)),
			SimpleLayerChromosome.of("DenseBlock",12,12), //LAST layer is fully connected layer, num of neurons = number of output classes
			SimpleLayerChromosome.of("Softmax",12,12),
			SimpleLayerChromosome.of("dataO",1,2)
		);
	
	@Override
	public Function<Genotype<LayerGene>, GenotypeTranslation> decoder() 
	{
		return gt-> new GenotypeTranslation(gt);
	}

	@Override
	public Factory<Genotype<LayerGene>> encoding() 
	{
		Configs.initialize();
		return Configs.ENCODING;
	}

}

