package nl.uva.aloha.helpers;

import java.io.File;
import java.util.Date;
import java.util.HashMap;

import org.bson.types.ObjectId;

import onnx.ONNX.ModelProto;
import espam.utils.fileworker.ONNXFileWorker;

import java.util.UUID;

public class OnnxRegistry 
{
	private static final OnnxRegistry singletonRegistryInstance = new OnnxRegistry();;
	private static HashMap<Integer, SearchResultCollector> Register;
	
	//public static String _onnxWorkspacePath = "/Users/sne/aloha_workspace/onnx/";
	public static String _onnxRootFolder;//= _onnxWorkspacePath +  new Date().getTime() + "/";
	public static String _onnxFolder;//= _onnxWorkspacePath +  new Date().getTime() + "/";
	
	
	private static long idCounter = 1;
	
	
	
	
	private OnnxRegistry()
	{
		Register = new HashMap<Integer, SearchResultCollector>();
	}
	
	private OnnxRegistry(String OnnxfolderPath)
	{
		super();
		_onnxFolder = OnnxfolderPath;
	}
	
	
	public static OnnxRegistry getInstance()
	{
		return singletonRegistryInstance;
	}
	
	static public SearchResultCollector saveOnnx(ModelProto model, String projectId)
	{
	  String onnx_fullPath;
	  onnx_fullPath = _onnxRootFolder + "/" + _onnxFolder;
		String onnxname = "o" + idCounter++ + ".onnx";
		File file = new File(onnx_fullPath);
		if (!file.exists()) 
		{
            System.out.print("No Folder:" + onnx_fullPath);
            file.mkdirs();
            System.out.println("-Folder created");
        }
	
		file = new File(onnx_fullPath + "/onnx");
		if (!file.exists()) 
		{
            System.out.print("No Folder:" + onnx_fullPath + "/onnx/");
            file.mkdirs();
            System.out.println("-Folder created");
        }
		
		ONNXFileWorker.writeModel(model, onnx_fullPath + "/onnx/" + onnxname);		
       
	//	ObjectId id = MongoDB.createAlgorithmConfiguration(projectId,  _onnxFolder + "/onnx/" + onnxname);
		SearchResultCollector srCol = new SearchResultCollector();
		
    System.out.println("aaaaaaaaaaaaaaaaaaa");
		srCol.id = UUID.randomUUID().toString();// id.toString();
		srCol.onnxName =  onnx_fullPath + "/onnx/" + onnxname;
		srCol.relativePath =  _onnxFolder + "/onnx/" + onnxname;
		return srCol;
        
        //TODO: db entry - get id, create resultCollector instance and 
	}
	
	public void addEntry(int GenotypeUniquecode,SearchResultCollector resultCollector)
	{
		if(hasEntry(new Integer(GenotypeUniquecode)))
			System.err.println("Duplicate Entry?");
		
		Register.put(new Integer(GenotypeUniquecode), resultCollector);
	}
	
	public Boolean hasEntry(int GenotypeUniquecode)
	{
		return Register.containsKey(new Integer(GenotypeUniquecode));
	}
	
	
	public SearchResultCollector getEntry(int GenotypeUniquecode)
	{
		return Register.get(new Integer(GenotypeUniquecode));
	}
}
