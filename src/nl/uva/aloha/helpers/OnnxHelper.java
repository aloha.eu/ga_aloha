package nl.uva.aloha.helpers;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.Date;

import espam.datamodel.graph.cnn.Network;
import espam.utils.fileworker.ONNXFileWorker;
import io.jenetics.Genotype;
import nl.uva.aloha.Alterers.ONNXAlteration;
import nl.uva.aloha.converters.GeneToOnnx;
import onnx.ONNX.ModelProto;

public class OnnxHelper 
{
	
	private static String _pythonLocalFilePath = "/src/nl/uva/aloha/pythonScripts/";
	private static String _pythonFileName = "Cifar10_onnx_shuffle.py";
	
	private static long idCounter = 1;
	
	private static int genDerived = 0;
	
	
	/**
	 * This class is now private - Functions of this class are replaces by functions in ONNXRegistry and Satellite evaluator
	 */
	private OnnxHelper()
	{
		
	}
	
	static public String saveGenotypeAsOnnx(Genotype gt)
	{
		ModelProto model =  new GeneToOnnx(gt).convertToONNXModel();
		return saveOnnx(model);
	}
	
	

	static public String saveNetworkAsOnnx(Network network)
	{
		ModelProto model =  new GeneToOnnx(network).convertToONNXModel();
		return saveOnnx(model);
	}
	
	static public String saveOnnx(ModelProto model)
	{
		String onnxname = "o" + idCounter++ + ".onnx";
		File file = new File(OnnxRegistry._onnxFolder);
		if (!file.exists()) 
		{
            System.out.print("No Folder:" + OnnxRegistry._onnxFolder);
            file.mkdir();
            System.out.println("Folder created" + new Date().toString());
        }
		
		ONNXFileWorker.writeModel(model, OnnxRegistry._onnxFolder + onnxname);		
		OnnxGarbageCollector.getInstance().addFileToListForGeneration(genDerived, onnxname);
        return onnxname;
	}
	
	static public Double evaluateOnnx(String onnxname)
	{
		
		return evaluateOnnx(onnxname, 1);
		
	}
	
	
	
	static public Double testOnnx(String onnxname)
	{
		ONNXAlteration onnxAlterGt = new ONNXAlteration(OnnxRegistry._onnxFolder + onnxname);
		onnxAlterGt.resetReshapeLayerDimForTesting();
		//onnxAlterGt.renameInputOutputLayerAfterOneRun();
		onnxAlterGt.updateONNXFile();
		
		File tempFile = new File("");
        String pythonScriptPath = tempFile.getAbsolutePath() + _pythonLocalFilePath + "Cifar10_onnx_test.py";
        
        String[] cmd = new String[3];
        cmd[0] = "python3";
        cmd[1] = pythonScriptPath;
        cmd[2] = OnnxRegistry._onnxFolder + onnxname;
       // cmd[3] = 
        
        try
        {
        	 Runtime rt = Runtime.getRuntime();
             Process pr = rt.exec(cmd);

             /** retrieve output from python script */
             BufferedReader bfr = new BufferedReader(new InputStreamReader(pr.getInputStream()));
             String line = "";
             String pythonScriptResult = "";
             while((line = bfr.readLine()) != null) {
                 pythonScriptResult+=line;
             }
             System.out.println("Test - " +pythonScriptResult);
             return new Double(pythonScriptResult);
        }
        catch(Exception e)
        {
        	System.err.println(e.getMessage());
        	return 0.0;
        }
	}
	
	static public Double evaluatePamap2Onnx(String onnxname, Integer generation)
	{
		genDerived = generation;
		File tempFile = new File("");
        String pythonScriptPath = tempFile.getAbsolutePath() + _pythonLocalFilePath + "Pamap2_training_cpu.py";
        
        String[] cmd = new String[4];
        cmd[0] = "python3";
        cmd[1] = pythonScriptPath;
        cmd[2] = OnnxRegistry._onnxFolder + onnxname;
        cmd[3] = generation.toString();
        
        try
        {
        	 Runtime rt = Runtime.getRuntime();
             Process pr = rt.exec(cmd);
             int exitVal = pr.waitFor();
             //System.out.print("Exit:" +  exitVal);
             
             /** retrieve output from python script */
             BufferedReader bfr = new BufferedReader(new InputStreamReader(pr.getInputStream()));
             String line = "";
             String pythonScriptResult = "";
             while((line = bfr.readLine()) != null) {
                 pythonScriptResult+=line;
             }
             if(exitVal == 1)
             {
	             BufferedReader bfr1 = new BufferedReader(new InputStreamReader(pr.getErrorStream()));
	             line = "";
	             String pythonScriptError= "";
	             while((line = bfr1.readLine()) != null) {
	            	 pythonScriptError+=(line+"/n");
	             }
	             System.out.println("Error - " +pythonScriptError);
	             return 0.0;
             }
             System.out.print("R-" +pythonScriptResult + " -- ");
           
             if(pythonScriptResult.split(":").length>1)
            	 return new Double(pythonScriptResult.split(":")[1]);
             else 
            	 return evaluateOnnx(onnxname,generation);
        }
        catch(Exception e)
        {
        	System.err.println(e.getMessage());
        	return 0.0;
        }
	}
	
	static public Double evaluateOnnx(String onnxname, Integer generation)
	{
		genDerived = generation;
		File tempFile = new File("");
        String pythonScriptPath = tempFile.getAbsolutePath() + _pythonLocalFilePath + "Cifar10_onnx_shuffle_gen.py";
        
        String[] cmd = new String[4];
        cmd[0] = "python3";
        cmd[1] = pythonScriptPath;
        cmd[2] = OnnxRegistry._onnxFolder + onnxname;
        cmd[3] = generation.toString();
        
        try
        {
        	 Runtime rt = Runtime.getRuntime();
             Process pr = rt.exec(cmd);
             int exitVal = pr.waitFor();
             //System.out.print("Exit:" +  exitVal);
             
             /** retrieve output from python script */
             BufferedReader bfr = new BufferedReader(new InputStreamReader(pr.getInputStream()));
             String line = "";
             String pythonScriptResult = "";
             while((line = bfr.readLine()) != null) {
                 pythonScriptResult+=line;
             }
             if(exitVal == 1)
             {
	             BufferedReader bfr1 = new BufferedReader(new InputStreamReader(pr.getErrorStream()));
	             line = "";
	             String pythonScriptError= "";
	             while((line = bfr1.readLine()) != null) {
	            	 pythonScriptError+=(line+"/n");
	             }
	             System.out.println("Error - " +pythonScriptError);
	             return 0.0;
             }
             //System.out.print("R-" +pythonScriptResult + " -- ");
           
             if(pythonScriptResult.split(":").length>1)
            	 return new Double(pythonScriptResult.split(":")[1]);
             else 
            	 return evaluateOnnx(onnxname,generation);
        }
        catch(Exception e)
        {
        	System.err.println(e.getMessage());
        	return 0.0;
        }
	}
	
}

