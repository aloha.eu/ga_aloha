package nl.uva.aloha.helpers;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import io.jenetics.Genotype;
import nl.uva.aloha.Configs;
import nl.uva.aloha.genetic.LayerGene;
import java.net.*;
import java.net.URL;
import java.net.HttpURLConnection;
import java.io.InputStream;
import java.io.DataOutputStream;
import java.util.UUID;

import redis.clients.jedis.Jedis; 

import nl.uva.aloha.helpers.RedisDB;

public class SatelliteEvaluator 
{
	
	public static String pythonScriptFolder = "/../tools_wrapper.py";
	
	private static String _accuracyEvaluatorName = "training";
	private static String _hardwareEvaluatorName = "power_perf";
	private static String _securityEvaluatorName = "security";
	private static String _rpiEvaluatorName = "rpi";
	
	
	public SatelliteEvaluator()
	{
		
	}
	
	public SatelliteEvaluator(String pythonscriptFolderPath)
	{
		pythonScriptFolder = pythonscriptFolderPath;
	}
	
	
	
//   ██████╗ █████╗ ██╗     ██╗     ██████╗  █████╗  ██████╗██╗  ██╗███████╗
//  ██╔════╝██╔══██╗██║     ██║     ██╔══██╗██╔══██╗██╔════╝██║ ██╔╝██╔════╝
//  ██║     ███████║██║     ██║     ██████╔╝███████║██║     █████╔╝ ███████╗
//  ██║     ██╔══██║██║     ██║     ██╔══██╗██╔══██║██║     ██╔═██╗ ╚════██║
//  ╚██████╗██║  ██║███████╗███████╗██████╔╝██║  ██║╚██████╗██║  ██╗███████║
//   ╚═════╝╚═╝  ╚═╝╚══════╝╚══════╝╚═════╝ ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝╚══════╝
//                                                                          

	
	
	public static String get_callback_id (){
	
    String callback_id = UUID.randomUUID().toString();

    RedisDB.set(callback_id,"1");
    
    return callback_id;

	}
	
	public static void free_callback_id(String callback_id){
    try{

      if (RedisDB.exists(callback_id))
        RedisDB.delete(callback_id); 
    }
    catch(Exception e)
		{
			System.err.println(e.getMessage());
			
		}
    }
  public static void barrier_cb (String callback_id){
  

    System.out.println ("Waiting for callback; id: " + callback_id);
    //wait for results. Query the redis DB for the exisistence of the callback_id key
    
    int done = 0;
    while (done==0){
      if (RedisDB.exists(callback_id))
        try {
          Thread.sleep(5000);
        }
	      catch(Exception e)
		    {
		   	System.err.println(e.getMessage());
	     	}
      else
        done = 1;
   }
    System.out.println ("Callback caugth.");
  }
  

//  ██╗  ██╗████████╗████████╗██████╗     ██████╗ ███████╗ ██████╗ 
//  ██║  ██║╚══██╔══╝╚══██╔══╝██╔══██╗    ██╔══██╗██╔════╝██╔═══██╗
//  ███████║   ██║      ██║   ██████╔╝    ██████╔╝█████╗  ██║   ██║
//  ██╔══██║   ██║      ██║   ██╔═══╝     ██╔══██╗██╔══╝  ██║▄▄ ██║
//  ██║  ██║   ██║      ██║   ██║         ██║  ██║███████╗╚██████╔╝
//  ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝         ╚═╝  ╚═╝╚══════╝ ╚══▀▀═╝ 
//                                                                 

	public static JSONObject executePost(String targetURL, String urlParameters) {
  HttpURLConnection connection = null;

  try {
    //Create connection
    URL url = new URL(targetURL);
    connection = (HttpURLConnection) url.openConnection();
    connection.setRequestMethod("POST");
    connection.setRequestProperty("Content-Type", 
        "application/json");

    connection.setRequestProperty("Content-Length", 
        Integer.toString(urlParameters.getBytes().length));
    connection.setRequestProperty("Content-Language", "en-US");  

    connection.setUseCaches(false);
    connection.setDoOutput(true);

    //Send request
    DataOutputStream wr = new DataOutputStream (
        connection.getOutputStream());
    wr.writeBytes(urlParameters);
    wr.close();

    //Get Response  
    InputStream is = connection.getInputStream();
    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
    StringBuilder response = new StringBuilder(); // or StringBuffer if Java version 5+
    String line;
    while ((line = rd.readLine()) != null) {
      response.append(line);
      response.append('\r');
    }
    rd.close();
    
    JSONParser parser = new JSONParser();
	  JSONObject resp=null;
    
    try
		{
  	  resp = (JSONObject) parser.parse(response.toString());
	  }
		catch(Exception e)
		{
			System.err.println(e.getMessage());
		}
    return resp;
  } catch (Exception e) {
    e.printStackTrace();
    return null;
  } finally {
    if (connection != null) {
      connection.disconnect();
    }
  }
}
	
	
//  ████████╗██████╗  █████╗ ██╗███╗   ██╗██╗███╗   ██╗ ██████╗ 
//  ╚══██╔══╝██╔══██╗██╔══██╗██║████╗  ██║██║████╗  ██║██╔════╝ 
//     ██║   ██████╔╝███████║██║██╔██╗ ██║██║██╔██╗ ██║██║  ███╗
//     ██║   ██╔══██╗██╔══██║██║██║╚██╗██║██║██║╚██╗██║██║   ██║
//     ██║   ██║  ██║██║  ██║██║██║ ╚████║██║██║ ╚████║╚██████╔╝
//     ╚═╝   ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝╚═╝  ╚═══╝╚═╝╚═╝  ╚═══╝ ╚═════╝ 
//                                                              

	
	
	public static JSONObject evaluateAccuracy(SearchResultCollector srCol)
	{
	  String callback_id = get_callback_id();
		
		String data;
		data = "";
    String url;
    url = "http://training:5000/api/training_call?project_id="+Configs.projectId+"&designpoint_id="+srCol.id+"&callback_id=" + callback_id;
	  JSONObject resp = executePost(url, data);
	  
	  return resp;

	}
	
	public static JSONObject evaluateAccuracy(Genotype<LayerGene> genotype)
	{
		SearchResultCollector srCol = OnnxRegistry.getInstance().getEntry(System.identityHashCode(genotype));		
		
		JSONObject resp = evaluateAccuracy(srCol);
		
	  return resp;
	}
	
	
	public static SearchResultCollector getAccuracyResult(SearchResultCollector srCol)
	{
	
    System.out.println("rrrrrrrrrrrrrrr2");
		try
		{
			srCol.accuracy = new Double(MongoDB.retrieveAlgorithmConfiguration(srCol.id, "validation_accuracy"));
    System.out.println("rrrrrrrrrrrrrrr3");
			return srCol;
		}
		catch(Exception e)
		{
			System.err.println(e.getMessage());
			srCol.accuracy = 0.0;
    System.out.println("rrrrrrrrrrrrrrr4");
			return srCol;
		}
	}
	
	public static SearchResultCollector getAccuracyResult(Genotype<LayerGene> genotype)
	{
	
		SearchResultCollector srCol = OnnxRegistry.getInstance().getEntry(System.identityHashCode(genotype));		
		return getAccuracyResult(srCol);
		
	}
	
	
	
//  ██████╗  ██████╗ ██╗    ██╗███████╗██████╗     ██████╗ ███████╗██████╗ ███████╗
//  ██╔══██╗██╔═══██╗██║    ██║██╔════╝██╔══██╗    ██╔══██╗██╔════╝██╔══██╗██╔════╝
//  ██████╔╝██║   ██║██║ █╗ ██║█████╗  ██████╔╝    ██████╔╝█████╗  ██████╔╝█████╗  
//  ██╔═══╝ ██║   ██║██║███╗██║██╔══╝  ██╔══██╗    ██╔═══╝ ██╔══╝  ██╔══██╗██╔══╝  
//  ██║     ╚██████╔╝╚███╔███╔╝███████╗██║  ██║    ██║     ███████╗██║  ██║██║     
//  ╚═╝      ╚═════╝  ╚══╝╚══╝ ╚══════╝╚═╝  ╚═╝    ╚═╝     ╚══════╝╚═╝  ╚═╝╚═╝     
//                                                                                 

	
	
	public static JSONObject evaluateHardware(SearchResultCollector srCol)
	{	
		String callback_id = get_callback_id();
		
		String data;
		data = "{ \"project_id\":\""        + Configs.projectId +
		       "\", \"algorithm_id\":\""    + srCol.id+
		       "\", \"architecture_id\":\"" + Configs.projectId+"\"}";
    String url;
    url = "http://power_perf:5000/api/power_performance_evaluations?callback_id=" + callback_id;
	  JSONObject resp = executePost(url, data);
	  
	  return resp;

	}
	
	public static JSONObject evaluateHardware(Genotype<LayerGene> genotype)
	{
		SearchResultCollector srCol = OnnxRegistry.getInstance().getEntry(System.identityHashCode(genotype));
		JSONObject resp = evaluateHardware(srCol);
	  return resp;
	}
	
	public static SearchResultCollector getHardwareResults(SearchResultCollector srCol)
	{
		try
		{
			srCol.performance  = new Double(MongoDB.retrieveAlgorithmConfiguration(srCol.id, "performance"));
			srCol.energy       = new Double(MongoDB.retrieveAlgorithmConfiguration(srCol.id, "energy"));
			srCol.memory       = new Double(MongoDB.retrieveAlgorithmConfiguration(srCol.id, "memory"));
			srCol.processors   = new Double(MongoDB.retrieveAlgorithmConfiguration(srCol.id, "processors")).intValue();
			return srCol;
		}
		catch(Exception e)
		{
			System.err.println(e.getMessage());
			srCol.performance = 0.0;
			srCol.energy  = 0.0;
			srCol.memory  = 0.0;
			srCol.processors  = 0;
			return srCol;
		}
		
	}
	
	
	public static SearchResultCollector getHardwareResults(Genotype<LayerGene> genotype)
	{
		SearchResultCollector srCol = OnnxRegistry.getInstance().getEntry(System.identityHashCode(genotype));	
		return getHardwareResults(srCol);
	}
	
	
	
	
	
	
	
	
	private static String callPythonFor(String callName, String id)
	{
		
		File tempFile = new File("");
		String pythonScriptPath = tempFile.getAbsolutePath() + pythonScriptFolder;
		
		String[] cmd = new String[5];
		cmd[0] = "python3";
		cmd[1] = pythonScriptPath;
		cmd[2] = callName;
		cmd[3] = "-i=" + Configs.projectId;
		cmd[4] = "-a=" + id;
		
		System.out.println("PythonCall " + pythonScriptPath + " " + callName);
		
		try
		{
		      System.out.println("Pyyyyyyyy1");
	    	 Runtime rt = Runtime.getRuntime();
	         Process pr = rt.exec(cmd);
	         BufferedReader bfr = new BufferedReader(new InputStreamReader(pr.getErrorStream()));
             String line = "";
             String pythonScriptResult = "";
             while((line = bfr.readLine()) != null) {
                 pythonScriptResult+=line;
             }
		      System.out.println("Pyyyyyyyy2");
             //if(!pythonScriptPath.isEmpty())
            	 System.out.println("pythonScriptResult: " + pythonScriptResult);
             return pythonScriptResult;
	    }
       
        catch(Exception e)
	    {
		      System.out.println("Pyyyyyyyy5");
        	System.err.println(e.getMessage());
        	return "";
	    }
	}

}
