package nl.uva.aloha.helpers;

/******************************************************************************
 *

 *  MongoDB test
 *
 * mongo-java-driver-3.10.2
 * 
 ******************************************************************************/

import com.mongodb.ConnectionString;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoClient;
//import com.mongodb.client.MongoClientURI;
//import com.mongodb.client.ServerAddress;

import com.mongodb.ServerAddress;
import com.mongodb.MongoClientSettings;

import org.slf4j.LoggerFactory;

import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoCollection;

import org.bson.Document;
import java.util.Arrays;
import com.mongodb.Block;

import com.mongodb.client.MongoCursor;
import static com.mongodb.client.model.Filters.*;
import com.mongodb.client.result.DeleteResult;
import static com.mongodb.client.model.Updates.*;
import com.mongodb.client.result.UpdateResult;
import java.util.ArrayList;
import java.util.List;

import com.mongodb.client.MongoIterable;

import org.bson.Document;
import org.bson.types.ObjectId;
import java.util.Arrays;
//import com.mongodb.Block;

import com.mongodb.client.MongoCursor;
import static com.mongodb.client.model.Filters.*;
//import com.mongodb.client.result.DeleteResult;
//import static com.mongodb.client.model.Updates.*;
//import com.mongodb.client.result.UpdateResult;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;

import org.bson.types.ObjectId;

public class MongoDB {
    
    private static String host = null;
    private static int port    = 0;
    private static MongoClient mongoClient = null;
    private static MongoDatabase database=null;
        
    public static void connectMongoDB (){
        ((LoggerContext) LoggerFactory.getILoggerFactory()).getLogger("org.mongodb.driver").setLevel(Level.ERROR);


        host = System.getenv("MONGO_DB_TCP_ADDR");
        if (host == null)
          host="localhost";
          
        try{
        port = Integer.parseInt(System.getenv("MONGO_DB_PORT"));
        }
        catch(Exception e){
          port=27018;
        }
        
        mongoClient = MongoClients.create(
        MongoClientSettings.builder()
                           .applyToClusterSettings(builder -> builder.hosts(Arrays.asList(new ServerAddress(host, port)))).build());
            
        // get a Database
        String dbname = System.getenv("MONGO_DB_NAME");
        if (dbname == null)
          dbname="aloha2";
            
        database = mongoClient.getDatabase(dbname);
            
    }

    public static String retrieveConstraints (String project_id)
    {
    	
    	//MongoClient mongoClient = null;
    	try
    	{

            //get a collection
            MongoCollection<Document> collection = database.getCollection("project_constraints");
            
            // filter the collection by project field and if many results are returned select the first
            Document myDoc = collection.find(eq("project", project_id)).first();
           // mongoClient.close();
           
            return myDoc.toJson();
           
    	}
    	catch(Exception e)
    	{
    		System.err.println(e.getMessage());
    		
    		return "{\"_id\": {\"$oid\": \"5cb5993075f2c5000b935d36\"}, \"creation_date\": {\"$date\": 1555405104890}, \"modified_date\": {\"$date\": 1555405104890}, \"power_value\": 0.5, \"power_priority\": 2, \"security_value\": 3, \"security_priority\": 1, \"execution_time_value\": 4, \"execution_time_priority\": 1, \"accuracy_value\": 3, \"accuracy_priority\": 1, \"project\": \"5cb5993075f2c5000b935d33\"}";   
    	}
    	//finally
    	//{
    		//if(mongoClient!=null)
    			//mongoClient.close();
    	//}
        
    }

    public static String retrieveProject (String project_id){
    
    	try
    	{
         //get a collection
         MongoCollection<Document> collection = database.getCollection("project");
            
         // filter the collection by project field and if many results are returned select the first
         Document myDoc = collection.find(eq("_id", new ObjectId(project_id))).first();
         // mongoClient.close();
         return myDoc.toJson();
    	}
    	catch(Exception e)
    	{
    	  System.out.println("Did you connect to the DB? Use the class method connectMongoDB()");
    		System.err.println(e.getMessage());
    		
    		return null;  
    	}
        
    }
    
    public static ObjectId createAlgorithmConfiguration (String project_id, String onnxPath){
    	
        Document algo = new Document("project", project_id)
                .append("onnx", onnxPath)
                .append("pytorch_net", "")
                .append("onnx_trained", "")
                .append("pytorch_trained", "")
                .append("onnx_trained", "")
                .append("training_accuracy", 0)
                .append("training_loss", 0)
                .append("validation_accuracy", 0)
                .append("validation_loss", 0)
                .append("training_log", "TODO: replace with array")
                .append("execution_time", "TODO: replace with array")
                .append("rpi_onnx_path", "")
                .append("performance", 0)
                .append("energy", 0)
                .append("processors", 2.0)
                .append("memory", 0)
                .append("sec_level", "")
                .append("sec_value", 0.0)
                .append("sec_curve", "TODO: replace with dict")
                .append("training_img_path", "")
                .append("power_perf_eval_img_path", "")
                .append("rpi_img_path", "")
                .append("security_img_path", "")
                .append("training_log_path", "")
                .append("power_perf_eval_log_path", "")
                .append("rpi_log_path", "")
                .append("security_log_path", "");
        
        
    	try
    	{
        //get a collection
        MongoCollection<Document> collection = database.getCollection("algorithm_configuration");
        
        collection.insertOne(algo);
      
        ObjectId id = (ObjectId) algo.get( "_id" );
        
       // System.out.println("Algo id");
       // System.out.println(id);
        
        return id;
    	}
    	catch(Exception e)
    	{
    	  System.out.println("Did you connect to the DB? Use the class method connectMongoDB()");
    		System.err.println(e.getMessage());
    		
    		return null;  
    	}
        
    }

    public static String retrieveInputShape (String project_id)
    {
        String res = null;
    //	try
    //	{
        //get a collection
        
        MongoCollection<Document> collection = database.getCollection("configuration_file");
        
        // filter the collection by project field and if many results are returned select the first
        Document myDoc = collection.find(eq("project", project_id)).first();
        res= myDoc.get("image_dimensions").toString();
        
    /*	}
    	catch(Exception e)
    	{
    	  System.out.println("Did you connect to the DB? Use the class method connectMongoDB()");
    		System.err.println(e.getMessage());
    		
    	}*/
        return res;
    	
    }
    
    public static String retrieveOutputShape (String project_id)
    {
        String res = null;
    //	try
    //	{
        //get a collection
        
        MongoCollection<Document> collection = database.getCollection("configuration_file");
        
        // filter the collection by project field and if many results are returned select the first
        Document myDoc = collection.find(eq("project", project_id)).first();
        res= myDoc.get("num_classes").toString();
        
    /*	}
    	catch(Exception e)
    	{
    	  System.out.println("Did you connect to the DB? Use the class method connectMongoDB()");
    		System.err.println(e.getMessage());
    		
    	}*/
        return res;
    	
    }
    
    public static String retrieveConfigurationFile (String project_id)
    {
        
    	try
    	{
        //get a collection
        
        
        MongoCollection<Document> collection = database.getCollection("configuration_file");
        
        // filter the collection by project field and if many results are returned select the first
        Document myDoc = collection.find(eq("project", project_id)).first();
        return myDoc.toJson();
    	}
    	catch(Exception e)
    	{
    	  System.out.println("Did you connect to the DB? Use the class method connectMongoDB()");
    		System.err.println(e.getMessage());
    		
    		return null;  
    	}
    }
    
    



    public static String retrieveAlgorithmConfiguration (String project_id)
    {
        
    	try
    	{
        //get a collection
        MongoCollection<Document> collection = database.getCollection("algorithm_configuration");
        
        // filter the collection by project field and if many results are returned select the first
        Document myDoc = collection.find(eq("project", project_id)).first();
        return myDoc.toJson();
    	}
    	catch(Exception e)
    	{
    	  System.out.println("Did you connect to the DB? Use the class method connectMongoDB()");
    		System.err.println(e.getMessage());
    		
    		return null;  
    	}
    }
    
    
    public static String retrieveAlgorithmConfiguration (String algorithm_id, String field)
    {
    	try
    	{
    	  
        //get a collection
        MongoCollection<Document> collection = database.getCollection("algorithm_configuration");
        
        // filter the collection by project field and if many results are returned select the first
        Document myDoc = collection.find(eq("_id", new ObjectId(algorithm_id))).first();
        String res= myDoc.get(field).toString();
        
        return res;
    	}
    	catch(Exception e)
    	{
    	  System.out.println("Did you connect to the DB? Use the class method connectMongoDB()");
    		System.err.println(e.getMessage());
    		
    		return null; 
    	}
       
    }
    

    
   public static void main(String[] args) {
        
        System.out.println("########################");
        System.out.println("##### MongoDB Test #####");
        System.out.println("########################\n\n");
        
        //connect to the mongo db server
        System.out.println("Connecting to the server...");
        connectMongoDB();
        
        
        // list all the collections, i.e. tables, in the DB
        MongoIterable<String> colls = database.listCollectionNames();
        for (String s : colls) {
          System.out.println(s);
        }
        
        //get a collection
        MongoCollection<Document> collection = database.getCollection("project_constraints");
        
        // print the number of documents in the collection, i.e. rows of the table
        System.out.println(collection.countDocuments());
        
        // get the first element of the collection
        Document myDoc = collection.find().first();
        System.out.println(myDoc.toJson());
        
        
        
        System.out.println(retrieveConstraints("5cb5993075f2c5000b935d33"));
        
        //createAlgorithmConfiguration("5cb5993075f2c5000b935d33");

    }

}

