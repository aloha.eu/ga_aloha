package nl.uva.aloha.helpers;

import redis.clients.jedis.Jedis; 


public class RedisDB {
    
    private static String host = null;
    private static int port    = 0;
    private static Jedis jedis = null;
    
    public static void connectRedisDB (){
    
        host = System.getenv("REDIS_HOST");
        if (host == null)
          host="localhost";
          
        try{
        port = Integer.parseInt(System.getenv("REDIS_PORT"));
        }
        catch(Exception e){
          port=27018;
        }
        
        try{
       jedis = new Jedis(host,port); 
        }
    catch(Exception e)
		{
			System.err.println(e.getMessage());
			
		}
    }
    
    public static String get (String key){
      
        try{
        return jedis.get(key);}
    catch(Exception e)
		{
			System.err.println(e.getMessage());
			return null;
		}
    }
    
    public static void set (String key, String value){
      
        try{
        jedis.set(key, value);}
    catch(Exception e)
		{
			System.err.println(e.getMessage());
			
		}
    }
    
    public static void delete (String key){
      
      try{
        jedis.del(key);
      }
      catch(Exception e)
		  {
			  System.err.println(e.getMessage());
			
		  }
    }


    public static boolean exists (String key){
      
     // try{
        return jedis.exists(key);
    //  }
    //  catch(Exception e)
		//  {
		//	  System.err.println(e.getMessage());
    //    return false;
		//  }
		  
		  
    }
}
