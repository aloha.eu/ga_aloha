package nl.uva.aloha;

import java.util.ArrayList;
import java.util.Date;
import java.util.DoubleSummaryStatistics;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Collectors;

import espam.datamodel.graph.cnn.Network;
import io.jenetics.Genotype;
import io.jenetics.Phenotype;
import io.jenetics.TournamentSelector;
import io.jenetics.TruncationSelector;
import io.jenetics.engine.Engine;
import io.jenetics.engine.EvolutionResult;
import io.jenetics.ext.moea.NSGA2Selector;
import io.jenetics.ext.moea.Vec;
import nl.uva.aloha.Alterers.GenotypeRepeater;
import nl.uva.aloha.Alterers.LayerGeneMutator;
import nl.uva.aloha.Alterers.NetworkRecombinator;
import nl.uva.aloha.Alterers.ONNXAlteration;
import nl.uva.aloha.Selectors.CustomTruncatationSelector;
import nl.uva.aloha.converters.GeneToNetwork;
import nl.uva.aloha.converters.GeneToOnnx;
import nl.uva.aloha.genetic.AlohaProblem;
import nl.uva.aloha.genetic.DNNCodec;
import nl.uva.aloha.genetic.LayerGene;
import nl.uva.aloha.helpers.OnnxGarbageCollector;
import nl.uva.aloha.helpers.OnnxHelper;
import nl.uva.aloha.helpers.OnnxRegistry;
import nl.uva.aloha.helpers.SatelliteEvaluator;
import nl.uva.aloha.helpers.SearchResultCollector;
import onnx.ONNX.ModelProto;


//FOR SATTELITE EVALUATIONS, use MultiObjGAMain.java. This class has not been modified to be suitable with monogodb
public class GAMain {

	
	static final int populationSize = 40;
	static final int numOfIterations = 50;
	static final int parallelism = 5; 
	static final int bestSurvivors = 1; 
	public static final boolean BatchNormAfterConv = false;
	public static final boolean seperableConv = false;
	
	//public static final String DATASET = "CIFAR";
	//public static final String DATASET = "PAMAP2";
	public static final String DATASET = "NONE";
	
	static final Engine<LayerGene, Double> SINGLE_OBJECTIVE_ENGINE = 
			Engine.builder(GAMain::accuracyFitness,DNNCodec.PAMAP2ENCODING)
			.populationSize(populationSize)
			.maximizing()
			.survivorsSize(0)
			.offspringSelector(new CustomTruncatationSelector<>(populationSize-bestSurvivors))
			//.survivorsSelector(new TruncationSelector<>(bestSurvivors))
			.alterers(new NetworkRecombinator<Double>(0.25),
					  new LayerGeneMutator<Double>(0.35,0.12),
					  new GenotypeRepeater<Double>())
			.genotypeValidator( gt -> (GAMain.isGenotypeValid(gt)) )
			.executor(new ForkJoinPool(parallelism))
			.build();
		
	
	
	private static Double bestAccuracyYet = 0.0;
	private static List<Phenotype<LayerGene, Double>> bestPopulationYet = new ArrayList<>();
	private static List<Phenotype<LayerGene, Double>> bestofEachGen = new ArrayList<>();
	
	
	static final Engine<LayerGene, Vec<double[]>> ENGINE =
			Engine.builder(new AlohaProblem())
				.populationSize(100)
				.offspringSelector(new TournamentSelector<>(5))
				.survivorsSelector(NSGA2Selector.ofVec())
				.minimizing()
				.build();
	public static void main(final String[] args) 
	{
		String projId = args[0];
		
		if((projId!=null) &(!projId.isEmpty()))
			Configs.projectId = projId;
		
		
		
		System.out.println("iterations - " + numOfIterations);
		final EvolutionResult best = (EvolutionResult)SINGLE_OBJECTIVE_ENGINE.stream()//new InitialPopulationCreator(populationSize).createPopulation() )
				.limit(numOfIterations)
				.peek(GAMain::update)
				.collect(EvolutionResult.toBestEvolutionResult());

			System.out.println("Best result is "+best.getBestFitness() + "---" + new Date().toString());
			
			String bestONNX = OnnxRegistry.getInstance().getEntry(System.identityHashCode(best.getBestPhenotype().getGenotype())).onnxName;
			ONNXAlteration onnxAlterGt = new ONNXAlteration(OnnxRegistry._onnxFolder+ bestONNX);
			
			onnxAlterGt.resetReshapeLayerDimForTesting();
			onnxAlterGt.updateONNXFile();
			
			
			System.out.println("ONNX: " + bestONNX);
			System.out.println("Best set:");
			
			Iterator<Phenotype<LayerGene, Double>> it = bestPopulationYet.iterator();
			while(it.hasNext())
			{
				Phenotype<LayerGene, Double> pt = it.next();
				String testOnnxName = OnnxRegistry.getInstance().getEntry(System.identityHashCode(pt.getGenotype())).onnxName;
				System.out.println(pt.getGeneration() + " : " + testOnnxName + " : " + 
									pt.getFitness());//+ ":" + OnnxHelper.testOnnx(testOnnxName));
			}
			
			 
	}
	
	private static void update (final EvolutionResult<LayerGene, Double> result)
	{
		//System.out.println("gen:" + result.getGeneration());
		DoubleSummaryStatistics stats = result.getPopulation().stream().mapToDouble(pt->pt.getFitness()).summaryStatistics();
		DoubleSummaryStatistics topstats = result.getPopulation().stream().mapToDouble(pt->pt.getFitness()).map(d->1.0-d).sorted().map(d->1.0-d).
				limit(50).summaryStatistics();
		
		System.out.println("gen: " + result.getGeneration() + "stats: " + stats.toString() + "top50 avg:" + topstats.getAverage());
		generationTrack = new Long(result.getGeneration() + 1);
		
		Double bestOfThisEvolution = result.getBestFitness();
		bestAccuracyYet = (bestOfThisEvolution > bestAccuracyYet)? bestOfThisEvolution : bestAccuracyYet;
		
		Phenotype<LayerGene, Double> pt1 = result.getBestPhenotype();
		String testOnnxName = OnnxRegistry.getInstance().getEntry(System.identityHashCode(pt1.getGenotype())).onnxName;
		System.out.println(pt1.getGeneration() + " : " + testOnnxName + " : " +  pt1.getFitness()+ ":" + OnnxHelper.testOnnx(testOnnxName));
		
		OnnxGarbageCollector.getInstance().removeFileFromDeletionList((int)(result.getGeneration()),
							OnnxRegistry.getInstance().getEntry(System.identityHashCode(result.getBestPhenotype().getGenotype())).onnxName);
		
		bestPopulationYet.addAll(result.getPopulation().stream().collect(Collectors.<Phenotype<LayerGene, Double>>toList()));
		bestPopulationYet = bestPopulationYet.stream().filter(pt->pt.getFitness() > (bestAccuracyYet-0.01)).collect(Collectors.<Phenotype<LayerGene, Double>>toList());
		
		if(result.getGeneration() == numOfIterations)
		{
			Iterator<Phenotype<LayerGene, Double>> it = result.getPopulation().iterator();
			while(it.hasNext())
			{
				Phenotype<LayerGene, Double> pt = it.next();
				testOnnxName = OnnxRegistry.getInstance().getEntry(System.identityHashCode(pt.getGenotype())).onnxName;
				System.out.println(pt.getGeneration() + " : " + testOnnxName + " : " + 
									pt.getFitness()+ ":" + OnnxHelper.testOnnx(testOnnxName));
			}
			
		}
		
		
		
	}
	
	private static Long generationTrack = (long)1;
	
	private static double accuracyFitness(final Genotype<LayerGene> gt) 
	{
		String onnxname = "";
		OnnxRegistry registry = OnnxRegistry.getInstance();
		int hashcode = System.identityHashCode(gt);
		
		if(registry.hasEntry(hashcode))
		{
			onnxname = registry.getEntry(hashcode).onnxName;
		}
		else
		{
			
			GeneToOnnx convertor = new GeneToOnnx(gt);
			if(convertor.getNetwork().checkConsistency())
			{
				ModelProto model = convertor.convertToONNXModel();
				SearchResultCollector src = OnnxRegistry.saveOnnx(model, Configs.projectId);
				OnnxRegistry.getInstance().addEntry(System.identityHashCode(gt), src);
			}
			else
				System.out.println("Inconsistent network" + gt.toString());
			
		}
		
		try
		{
			Double accuracyResult = 0.0;
			if(DATASET.equals("PAMAP2"))
				accuracyResult = OnnxHelper.evaluatePamap2Onnx(onnxname, generationTrack.intValue());
			else
				accuracyResult = OnnxHelper.evaluateOnnx(onnxname, generationTrack.intValue());
			if(accuracyResult!=null)
			{
				 System.out.println(onnxname + ":" + gt.hashCode() +":" + accuracyResult.toString());
				 return accuracyResult;
			}
			else 
			{
				System.err.println("Accuracy Result is null");
				return 0.0;
			}
		}
		catch(Exception e)
		{
			System.err.print(e.getStackTrace());
			return 0.0;
		}
			
	
	}
	
	private static Boolean isGenotypeValid(Genotype<LayerGene> gt)
	{
		if(!( gt ).isValid())
			return false;
		
		Network network = GeneToNetwork.createNetworkFromGenotype(gt);
		if(network == null) 
			return false;
		
		if (network.checkConsistency())
			return true;
		else return false;
		
	}
}
