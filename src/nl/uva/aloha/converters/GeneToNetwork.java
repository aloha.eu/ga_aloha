package nl.uva.aloha.converters;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import espam.datamodel.graph.cnn.Layer;
import espam.datamodel.graph.cnn.Network;
import espam.datamodel.graph.cnn.neurons.arithmetic.Arithmetic;
import espam.datamodel.graph.cnn.neurons.cnn.Convolution;
import espam.datamodel.graph.cnn.neurons.cnn.Pooling;
import espam.datamodel.graph.cnn.neurons.simple.DenseBlock;
import espam.datamodel.graph.csdf.datasctructures.Tensor;
import io.jenetics.AbstractChromosome;
import io.jenetics.Chromosome;
import io.jenetics.Genotype;
import nl.uva.aloha.Configs;
import nl.uva.aloha.GAMain;
import nl.uva.aloha.genetic.LayerGene;

public class GeneToNetwork 
{
	 
	
	//DOES NOT SPECIFY CONNECTION TYPE 
	//TODO: Add connectionParameters
	private static Network createNetworkFromLayerGenes(List<?> layers)
	{
		Network network = new Network();
		int i =0;
		try
		{
				if(layers.get(0) instanceof LayerGene )
				{
					network.stackLayerTop("input", ((LayerGene)layers.get(0)).getAllele().getNeuron(),1);
				}
				
		}
		catch(Exception e) {
			System.err.println(e.getMessage());
			return null;
		}
		
		Layer lastMaxpooloutput = null;
		Layer lastskipConv = null;
		
		for(i=1;i<layers.size();i++) 
		{
			if(layers.get(i) instanceof LayerGene )
			{
				Layer la = ((LayerGene)layers.get(i)).getAllele();
				
				try 
				{
					if( (la.getNeuron() instanceof DenseBlock)  && (i<layers.size()-1) )
					{
						Layer nextLayer = ((LayerGene)layers.get(i+1)).getAllele();
						if((nextLayer.getName().contains("softmax")))
						{	
							DenseBlock db = (DenseBlock)la.getNeuron();
							db.setNeuronsNum(nextLayer.getNeuronsNum());
						}
						network.stackLayer(la.getName(),la.getNeuron(),la.getNeuronsNum(),la.getPads());
					}
					else if(la.getNeuron() instanceof Pooling)
					{
						lastMaxpooloutput = la;
						network.stackLayer(la.getName(),la.getNeuron(),la.getNeuronsNum(),la.getPads());
					}
					else if ((la.getNeuron() instanceof Convolution) && ( la.getName().startsWith("skip")))
					{
						lastskipConv = la;
						network.addLayer(la.getName(),la.getNeuron(),la.getNeuronsNum(),la.getPads());
						if(lastMaxpooloutput!=null)
							network.addConnection(lastMaxpooloutput.getName(), la.getName());
						lastMaxpooloutput = null;
					}
					else if(la.getNeuron() instanceof Arithmetic)
					{
						Layer prevLayer = null;
						if(i>2)
							prevLayer = ((LayerGene)layers.get(i-2)).getAllele();
						network.addLayer(la.getName(),la.getNeuron(),prevLayer.getNeuronsNum(),la.getPads());
		
						if(lastskipConv!=null)
							network.addConnection(lastskipConv.getName(), la.getName());
						if(prevLayer != null)
							network.addConnection(prevLayer.getName(), la.getName());
					}
					else
						network.stackLayer(la.getName(),la.getNeuron(),la.getNeuronsNum(),la.getPads());
				}
				catch(Exception e) {
					System.err.println(e.getMessage());
				}
			}
		}
		network.setInputLayer(network.getLayers().firstElement());
		network.setOutputLayer(network.getLayers().lastElement());
        
		try {
		//TODO: Input data is set for CIFAR here. update this to reflect other datasets.
		
				Tensor inputDataShapeExample = Configs.INPUT_DATA_SHAPE;
				
				//Tensor inputDataShapeExample = Configs.INPUT_DATA_CIFAR_SHAPE;
				//if(GAMain.DATASET.equals("PAMAP2"))
					 //inputDataShapeExample = Configs.INPUT_PAMAP2_SLIDING_SHAPE;
				network.setDataFormats(inputDataShapeExample);
		}
		catch(Exception e) {
			System.err.println(e.getMessage());
		}
		
		return network;
	}
	
	
	public static Network createNetworkFromGenotype(Genotype<LayerGene> gt)
	{ 
		Network network = null;
		ArrayList<LayerGene> layerGenes = new ArrayList<>();
		
		for (Iterator<Chromosome<LayerGene>> i = gt.iterator(); i.hasNext(); ) 
		{
			AbstractChromosome<LayerGene> ac = (AbstractChromosome<LayerGene>)(i.next());
			for (Iterator<LayerGene> j = ac.iterator(); j.hasNext(); ) 
			{
				LayerGene lg = j.next();
				if(lg.getLayer().getName().startsWith("skip"))
				{
					lg.getLayer().setNeuronsNum(ac.getGene(ac.length()-4).getLayer().getNeuronsNum()); // from reverse - add, skip, relu, conv. totalLen-4 is conv
					layerGenes.add(layerGenes.size()-1, lg);
				}
				else if(lg.getLayer().getName().startsWith("add"))
					layerGenes.add(layerGenes.size()-1, lg);
				else
					layerGenes.add(lg);
			}
		}
		network =createNetworkFromLayerGenes(layerGenes);
		
		//Tensor inputDataShapeExample = new Tensor(32,32,3);
		//network.setDataFormats(inputDataShapeExample);
		
		return network;
	}
}
