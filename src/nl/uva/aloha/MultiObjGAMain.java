package nl.uva.aloha;

import java.util.ArrayList;
import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Collectors;

import org.json.simple.JSONObject;

import io.jenetics.Phenotype;
import io.jenetics.engine.Engine;
import io.jenetics.engine.EvolutionResult;
import io.jenetics.ext.moea.MOEA;
import io.jenetics.ext.moea.NSGA2Selector;
import io.jenetics.ext.moea.Vec;
import io.jenetics.util.ISeq;
import nl.uva.aloha.Alterers.GenotypeRepeater;
import nl.uva.aloha.Alterers.LayerGeneMutator;
import nl.uva.aloha.Alterers.NetworkRecombinator;
import nl.uva.aloha.genetic.AlohaProblem;
import nl.uva.aloha.genetic.LayerGene;
import nl.uva.aloha.helpers.OnnxGarbageCollector;
import nl.uva.aloha.helpers.OnnxHelper;
import nl.uva.aloha.helpers.OnnxRegistry;
import nl.uva.aloha.helpers.SatelliteEvaluator;

import espam.datamodel.graph.csdf.datasctructures.Tensor;



import java.net.*;
import java.net.URL;
import java.net.HttpURLConnection;
import java.io.InputStream;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.util.UUID;


import redis.clients.jedis.Jedis; 

import nl.uva.aloha.helpers.MongoDB;
import nl.uva.aloha.helpers.RedisDB;

public class MultiObjGAMain 
{

	static final Engine<LayerGene, Vec<double[]>> ENGINE =
			Engine.builder(new AlohaProblem())
				.populationSize(Configs.populationSize)
				.minimizing()
				.survivorsSize(Configs.bestSurvivors)
				.offspringSelector(NSGA2Selector.ofVec())
				.alterers(new NetworkRecombinator<Vec<double[]>>(0.2),
						  new LayerGeneMutator<Vec<double[]>>(0.3, 0.15),
						  new GenotypeRepeater<Vec<double[]>>())
				.executor(new ForkJoinPool(Configs.parallelism))
				.build();
	
	
	public static JSONObject configObject;
	
	
	
	public static void main(final String[] args) 
	{
		try {
			MongoDB.connectMongoDB();
		}
		catch(Exception e) {
			System.out.println("Unable to connect to MongoDB");
		}
		
		try {
			RedisDB.connectRedisDB();
		}
		catch(Exception e) {
			System.out.println("Unable to connect to RedisDB");
		}
		
		String projId = args[0];
		
    
		System.out.println(projId);
		String inputshape;
		try {
			inputshape = MongoDB.retrieveInputShape(projId);
		}
		catch(Exception e) {
			System.out.println("No connection to MongoDB available. Using default.");
		  inputshape = "[32,32,3]";
		}
		
		System.out.println(inputshape);
		
		int[] in_shape = new int[3];
		
		in_shape [0] = Integer.parseInt(inputshape.replace("[","").replace("]","").split(",")[0].trim());
		in_shape [1] = Integer.parseInt(inputshape.replace("[","").replace("]","").split(",")[1].trim());
		in_shape [2] = Integer.parseInt(inputshape.replace("[","").replace("]","").split(",")[2].trim());
		
		
		Configs.INPUT_DATA_SHAPE = new Tensor (in_shape[1],in_shape[0],in_shape[2]);
		String outputshape;
		try {
		  outputshape = MongoDB.retrieveOutputShape(projId);
		}
		catch(Exception e) {
		  outputshape = "10";
		}
		Configs.OUTPUT_DATA_SHAPE = new Tensor (Integer.parseInt(outputshape));
		
		
		if((projId!=null) &(!projId.isEmpty()))
			Configs.projectId = projId;
		
		String onnxfolderPath = args[1];
		if((onnxfolderPath==null) ||(onnxfolderPath.isEmpty()))
			onnxfolderPath = "/Users/sne/aloha_workspace/temponnx/";
			
		OnnxRegistry._onnxRootFolder = onnxfolderPath;
		OnnxRegistry._onnxFolder = "experiments/prj_" + Configs.projectId ;
		System.out.println("aaa 2");
	
		
		//ALSO SET  SatelliteEvaluator.pythonScriptFolder from args [2]
		
		final ISeq<Phenotype<LayerGene,Vec<double[]>>> front = ENGINE.stream()
				.limit(Configs.numOfGAIterations)
				.peek(MultiObjGAMain::update)
				.collect(MOEA.toParetoSet(Configs.paretoSetSize));
				
		
	try{
		RedisDB.set(projId+":GAONNX:done","1");
			}
		catch(Exception e) {
		System.out.println("No connection to RedisDB available");
		
		}
			System.out.println(front.toString());
	}

	private static Long generationTrack = (long)1;
	private static Double bestAccuracyYet = 0.0;
	private static List<Phenotype<LayerGene, Vec<double[]>>> bestPopulationYet = new ArrayList<>();
	
	/**
	 * Called after every iteration to "peek" into the GA process.
	 * Things like statistics and tesing the best one with test set etc. can be done here
	 * 
	 */
	private static void update (final EvolutionResult<LayerGene, Vec<double[]>> result)
	{
		
		
		
		DoubleSummaryStatistics stats = result.getPopulation().stream().mapToDouble(pt->pt.getFitness().data()[0]).summaryStatistics();
		DoubleSummaryStatistics topstats = result.getPopulation().stream().mapToDouble(pt->pt.getFitness().data()[0]).map(d->1.0-d).sorted().map(d->1.0-d).
				limit(50).summaryStatistics();

		//TOPSTATS - here it is set to the result of top 50 individuals. Comment the above line if not needed.
		System.out.println("gen: " + result.getGeneration() + "stats: " + stats.toString() + "top50 avg:" + topstats.getAverage());
		
		//The best way to track generation, only problem is in first two iterations - TODO: confirm this.
		generationTrack = new Long(result.getGeneration() + 1);
		
		
		
		double bestOfThisEvolution = result.getBestFitness().data()[0];
		bestAccuracyYet = (bestOfThisEvolution > bestAccuracyYet)? bestOfThisEvolution : bestAccuracyYet;
		
		Phenotype<LayerGene, Vec<double[]>> pt1 = result.getBestPhenotype();
		//Testing the best of this generation with test set. All reported numbers during training are supposed to be from validation set. 
		//This test is just to "peek" into result  - The result does not (should not!) effect the GA or training process. 
		//String testOnnxName = OnnxRegistry.getInstance().getEntry(System.identityHashCode(pt1.getGenotype())).onnxName;
		//System.out.println(pt1.getGeneration() + " : " + testOnnxName + " : " +  pt1.getFitness()+ ":" + OnnxHelper.testOnnx(testOnnxName));
		
		//Automatically files are removed from the file system after some iterations --> 
		OnnxGarbageCollector.getInstance().removeFileFromDeletionList((int)(result.getGeneration()),
							OnnxRegistry.getInstance().getEntry(System.identityHashCode(result.getBestPhenotype().getGenotype())).onnxName);
		
		bestPopulationYet.addAll(result.getPopulation().stream().collect(Collectors.<Phenotype<LayerGene, Vec<double[]>>>toList()));
		bestPopulationYet = bestPopulationYet.stream().filter(pt->pt.getFitness().data()[0] > (bestAccuracyYet-0.01)).collect(Collectors.<Phenotype<LayerGene, Vec<double[]>>>toList());
		
		
		
		//If you want to do something on final iteration, use this IF block.
		/*if(result.getGeneration() == Configs.numOfGAIterations)
		{
			Iterator<Phenotype<LayerGene, Vec<double[]>>> it = result.getPopulation().iterator();
			while(it.hasNext())
			{
				Phenotype<LayerGene, Vec<double[]>> pt = it.next();
				testOnnxName = OnnxRegistry.getInstance().getEntry(System.identityHashCode(pt.getGenotype())).onnxName;
				System.out.println(pt.getGeneration() + " : " + testOnnxName + " : " + 
									pt.getFitness()+ ":" + OnnxHelper.testOnnx(testOnnxName));
			}
			
		}*/
		
		
		
	}
	
	
}
