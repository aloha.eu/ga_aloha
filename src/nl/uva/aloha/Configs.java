package nl.uva.aloha;

import java.io.FileReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import espam.datamodel.graph.csdf.datasctructures.Tensor;
import io.jenetics.Chromosome;
import io.jenetics.Genotype;
import io.jenetics.util.IntRange;
import nl.uva.aloha.genetic.DualLayerChromosome;
import nl.uva.aloha.genetic.LayerGene;
import nl.uva.aloha.genetic.SimpleLayerChromosome;

public class Configs {

	private static Configs singleton = null;
	
	public static String projectId = "a1";
	public static  int populationSize = 2;
	public static  int numOfGAIterations = 2;
	public static  int parallelism = 3; //depends on resources available - GPUs etc. 
	public static  int bestSurvivors = 0; 
	public static  IntRange paretoSetSize = IntRange.of(10, 30);
	public static final boolean BatchNormAfterConv = true; //Automatically adds batchnorm layer after convolution
	public static  int discreteLevel = 16;
	public static double mutationRate = 0.3;
	public static double crossoverRate = 0.3;
	
	public static int NUM_OUTPUT_CLASSES = 10;
	static public Tensor INPUT_DATA_CIFAR_SHAPE = new Tensor(32,32,3);
	static public Tensor INPUT_PAMAP2_SLIDING_SHAPE = new Tensor(100,1,40);
	static public Tensor INPUT_DATA_SHAPE = null;
	static public Tensor OUTPUT_DATA_SHAPE = null;
	
    public static int serverPollingIntervalMinutes = 1; //minutes
	public static String pathToDSJSON = "./src/nl/uva/aloha/designspace.json";
	
	public static Genotype<LayerGene> ENCODING;
    
  //what results to collect or use in multiobj?
    
    public static Configs getSingleton()
    {
    	if(singleton == null)
    		singleton = new Configs();
    	return singleton;
    }
    
    public static void initialize()
    {
    	if(singleton == null)
    		singleton = new Configs();
    }
    
	public Configs()
	{
		ArrayList<Chromosome<LayerGene>> chromosomes = new ArrayList<>();
		chromosomes.add(SimpleLayerChromosome.of("dataI",1,2));
		
		JSONParser parser = new JSONParser();
		 try (Reader reader = new FileReader(pathToDSJSON) ) 
		 {

	            JSONObject jsonObject = (JSONObject) parser.parse(reader);
	            //System.out.println(jsonObject);

	            String inputFormat = (String) jsonObject.get("inputFormat");
	            //System.out.println(inputFormat);

//	            NUM_OUTPUT_CLASSES =  new Long((String)(jsonObject.get("outputClasses").toString())).intValue();
	            NUM_OUTPUT_CLASSES = (int)(long)jsonObject.get("outputClasses");
	          

	            // loop array
	            JSONArray layers = (JSONArray) jsonObject.get("layers");
	            Iterator<JSONObject> iterator = layers.iterator();
	            while (iterator.hasNext()) 
	            {
	            	JSONObject layer =iterator.next();
	            	String type = (String) layer.get("type");
	            	if(layer.containsKey("dual"))
	            	{
	            		type = type + ":" + (String) layer.get("dual");
	            		if(layer.containsKey("minUnits"))
	            		{
		            		if(layer.containsKey("minLayers"))
		            			chromosomes.add(DualLayerChromosome.of(type, (int)(long)layer.get("minUnits"),(int)(long)layer.get("maxUnits"), 
		            							IntRange.of((int)(long)layer.get("minLayers")*2,(int)(long)layer.get("maxLayers")*2)));
		            		else
		            			chromosomes.add(DualLayerChromosome.of(type,(int)(long) layer.get("minUnits"),(int)(long) layer.get("maxUnits"),IntRange.of(2,3))); //ONLY one layer (its dual layer so in implementation it is 2 layers)
	            		}
	            		//IF a dual layer does not have minUnits is it going to be a valid gene?
	            	}
	            	else
	            	{
	            		if(layer.containsKey("minUnits"))
	            		{
		            		if(layer.containsKey("minLayers"))
		            			chromosomes.add(SimpleLayerChromosome.of(type, (int)(long)layer.get("minUnits"),(int)(long) layer.get("maxUnits"), 
		            					        IntRange.of((int)(long)layer.get("minLayers"),(int)(long)layer.get("maxLayers"))));
		            		else
		            			chromosomes.add(DualLayerChromosome.of(type,(int)(long) layer.get("minUnits"),(int)(long) layer.get("maxUnits"))); //ONLY one layer 
	            		}
	            		else
	            			chromosomes.add(SimpleLayerChromosome.of(type,5,50)); //Random units to initialise the chromosome. In implementations it doesnot matter what number is passed.
	            	}
	            }

	            chromosomes.add(SimpleLayerChromosome.of("DenseBlock",NUM_OUTPUT_CLASSES,NUM_OUTPUT_CLASSES));
	            chromosomes.add(SimpleLayerChromosome.of("Softmax",NUM_OUTPUT_CLASSES,NUM_OUTPUT_CLASSES));
	            chromosomes.add(SimpleLayerChromosome.of("dataO",1,2));
				
	            ENCODING = Genotype.of(chromosomes);
	     } 
		 catch (Exception e) 
		 {
	            e.printStackTrace();
	     }

	}
	
	public Configs getConfigs()
	{
		return singleton;
	}
	
	public static void main(final String[] args) 
	{
	
		Configs.initialize();
	}
	
	
	
}
