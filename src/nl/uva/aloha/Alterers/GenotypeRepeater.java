package nl.uva.aloha.Alterers;

import io.jenetics.AbstractAlterer;
import io.jenetics.AltererResult;
import io.jenetics.Genotype;
import io.jenetics.Phenotype;
import io.jenetics.util.MSeq;
import io.jenetics.util.Seq;
import nl.uva.aloha.Configs;
import nl.uva.aloha.converters.GeneToOnnx;
import nl.uva.aloha.genetic.LayerGene;
import nl.uva.aloha.helpers.OnnxHelper;
import nl.uva.aloha.helpers.OnnxRegistry;
import nl.uva.aloha.helpers.SearchResultCollector;
import onnx.ONNX.ModelProto;

public class GenotypeRepeater< T extends Comparable<? super T>>
	extends AbstractAlterer<LayerGene, T>
{

    //private final Factory<Genotype<G>> _factory;

    public GenotypeRepeater() 
    {
    	super(1.0);
    }

	@Override
	public AltererResult<LayerGene, T> alter(Seq<Phenotype<LayerGene, T>> population, long generation) 
	{
		final MSeq<Phenotype<LayerGene, T>> pop = MSeq.of(population);
		OnnxRegistry registry = OnnxRegistry.getInstance();
		
		int alterations =0;
		for (int i = 0, n = pop.size(); i < n; ++i) 
		{
			Phenotype<LayerGene, T> pt = pop.get(i); 
			if (pt.isEvaluated()) 
			{
				alterations++;
				
				String onnxOldGT = registry.getEntry(System.identityHashCode(pt.getGenotype())).onnxName;
				
				Genotype<LayerGene> gt = Genotype.of(pt.getGenotype().toSeq());
				
				GeneToOnnx convertor = new GeneToOnnx(gt);
				ModelProto model = convertor.convertToONNXModel();
				SearchResultCollector src = OnnxRegistry.saveOnnx(model, Configs.projectId);
				
		System.out.println("genotipe");
				String onnxNewGT = src.onnxName;
				OnnxRegistry.getInstance().addEntry(System.identityHashCode(gt), src);
			
				
				System.out.println("onnxNewGT");
				System.out.println( onnxNewGT);
				
				ONNXAlteration onnxAlterGt = new ONNXAlteration(onnxNewGT);
				
				System.out.println("onnxOldGT");
				System.out.println(onnxOldGT);
				onnxAlterGt.mutateFrom(onnxOldGT); 
				onnxAlterGt.updateONNXFile();
				
				Phenotype<LayerGene, T> ptnew = pt.newInstance(gt,generation);
				pop.set(i, ptnew);
				System.out.println("Repeated from- " + onnxOldGT  +" -To-" + onnxNewGT + ":" + ptnew.isEvaluated());
				
			}
		}
		
		return AltererResult.of(pop.toISeq(),alterations);
	}
}



