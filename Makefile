build: 
	mkdir -p bin
	# workaround to crosscompile against JDK8 that is included in docker images.
	javac -cp "./lib/*:./src" -d ./bin -source 1.8 -target 1.8 src/nl/uva/aloha/MultiObjGAMain.java
	javac -cp "./lib/*:./src" -d ./bin -source 1.8 -target 1.8 src/nl/uva/aloha/helpers/*
	javac -cp "./lib/*:./src" -d ./bin -source 1.8 -target 1.8 src/nl/uva/aloha/converters/*
	javac -cp "./lib/*:./src" -d ./bin -source 1.8 -target 1.8 src/nl/uva/aloha/genetic/*
	javac -cp "./lib/*:./src" -d ./bin -source 1.8 -target 1.8 src/nl/uva/aloha/Selectors/*
	javac -cp "./lib/*:./src" -d ./bin -source 1.8 -target 1.8 src/nl/uva/aloha/Alterers/*
